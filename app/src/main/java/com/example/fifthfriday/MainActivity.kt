package com.example.fifthfriday

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.i
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.children
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fifthfriday.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val viewModel: InfoViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: RecyclerViewAdapter

    private val saveInfo = mutableMapOf<String, String>()

    lateinit var inputText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observe()
        initItems()
    }

    private fun initItems(){

        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

        viewModel.init()

        register()

    }

    private fun register(){
        binding.register.setOnClickListener{
            checkFields()
            i("myMap", "$saveInfo")

        }
    }


    private fun observe(){
        viewModel._infoLiveData.observe(this, {
            adapter.setItems(it)
        })
    }

    private fun checkFields(){
        binding.recyclerView.children.forEach {
            repeat(it.findViewById<RecyclerView>(R.id.recyclerViewInside).children.count()) {
                inputText = findViewById(R.id.edTItem)
                if (inputText.text.toString().isNotEmpty()) {
                    saveInfo[inputText.hint.toString()] = inputText.text.toString()
                }
            }
        }

        Toast.makeText(this, "Information Saved", Toast.LENGTH_SHORT).show()
    }
}