package com.example.fifthfriday

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fifthfriday.databinding.ChoosableItemInsideLayoutBinding
import com.example.fifthfriday.databinding.ItemInsideLayoutBinding

class InsideRecyclerViewAdapter(private val items: MutableList<ItemsModelSubListItem>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val CHOOSER = 1
        private const val INPUT_ITEM = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == INPUT_ITEM)
            InsideItemViewHolder(
                ItemInsideLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            ChooserInsideItemViewHolder(
                ChoosableItemInsideLayoutBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )


    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is InsideItemViewHolder -> holder.bind()
        }
    }

    override fun getItemCount() = items.size

    inner class InsideItemViewHolder(private val binding: ItemInsideLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var item: ItemsModelSubListItem
        fun bind() {
            item = items[adapterPosition]
            binding.edTItem.hint = item.hint
            checkEditText()
        }

        fun checkEditText(){
            binding.edTItem.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    if (item.required.toBoolean()){
                        if (s.isNullOrBlank()){
                            binding.edTItem.error = "Fill this field"
                        }
                    }
                }

            })
        }
    }

    inner class ChooserInsideItemViewHolder(private val binding: ChoosableItemInsideLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun getItemViewType(position: Int): Int {
        val model = items[position]
        return if (model.fieldType == "input") INPUT_ITEM
        else CHOOSER
    }
}