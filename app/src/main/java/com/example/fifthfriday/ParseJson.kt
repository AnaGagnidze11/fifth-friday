package com.example.fifthfriday

import retrofit2.Response
import retrofit2.http.GET

interface ParseJson {
    @GET("/v3/d531f5f5-180d-4364-bae7-791dae87f732")
    suspend fun getInfo(): Response<MutableList<MutableList<ItemsModelSubListItem>>>
}