package com.example.fifthfriday

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

object ParsingService {

    private const val BASE_URL = "https://run.mocky.io/"

    fun retrofitService(): ParseJson {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(ParseJson::class.java)
    }
}