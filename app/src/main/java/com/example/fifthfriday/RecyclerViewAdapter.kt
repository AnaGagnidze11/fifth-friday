package com.example.fifthfriday

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fifthfriday.databinding.ItemLayoutBinding

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    private val items = mutableListOf(mutableListOf<ItemsModelSubListItem>())
    private val viewPool = RecyclerView.RecycledViewPool()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val childLayoutManager = LinearLayoutManager(holder.recyclerView.context)
        childLayoutManager.initialPrefetchItemCount = 4

        holder.recyclerView.apply {
            layoutManager = childLayoutManager
            adapter = InsideRecyclerViewAdapter(items[position])
            setRecycledViewPool(viewPool)

        }
    }

    override fun getItemCount()= items.size

    inner class ItemViewHolder(private val binding: ItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        val recyclerView: RecyclerView = binding.recyclerViewInside
    }


    fun setItems(items: MutableList<MutableList<ItemsModelSubListItem>>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}