package com.example.fifthfriday

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class InfoViewModel : ViewModel() {

    private val infoLiveData = MutableLiveData<MutableList<MutableList<ItemsModelSubListItem>>>().apply {
        mutableListOf<ItemsModelSubListItem>()
    }

    val _infoLiveData: LiveData<MutableList<MutableList<ItemsModelSubListItem>>> = infoLiveData



    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getInfo()
            }
        }
    }


    private suspend fun getInfo() {
        val result = ParsingService.retrofitService().getInfo()
        if (result.isSuccessful) {
            val items = result.body()
            infoLiveData.postValue(items)
        }
    }
}